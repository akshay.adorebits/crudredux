import 'react-native-gesture-handler';
import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

/* Screens */
import HomeScreen from '../screens/HomeScreen';
import AddEmployeesScreen from '../screens/AddEmployeesScreen';

/* reducers */
//import noteReducer from '../reducers/noteReducer';

const Stack = createStackNavigator();

const RootNavigation = () => {
  return (
      <Stack.Navigator>
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{ title: 'Welcome' }}
        />
        <Stack.Screen 
          name="AddEmployeesScreen" 
          component={AddEmployeesScreen}
          options={{ title: 'Add Employee' }}
        />
      </Stack.Navigator>
  );
};

export default RootNavigation;