import React , { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    TextInput
} from 'react-native';
import {connect} from 'react-redux';
import {createEmployee, updateEmployee} from '../actions/Employees';

class AddEmployeesScreen extends Component {
    constructor(props){
        //super(props)
        super()
        this.state = {
            name:'',
            salary : '',
            age : ''
        } 
    }

    componentDidMount() {
        if(this.props.route.params){
            // const { id } = this.props.route.params;
             
            const { employee } = this.props.route.params;
            //console.log(employee.employee_name)
            this.setState({id : employee.id})
            this.setState({name : employee.employee_name})
            this.setState({salary : employee.employee_salary})
            this.setState({age : employee.employee_age})
           
        }
    }
    saveData(){
        //alert()
        var name = this.state.name;
        var salary = this.state.salary;
        var age = this.state.age;

        if(name.length >0 && salary.length>0 && age.length > 0){

            if(this.state.id == ''){
                var formData = {
                    name : name,
                    salary : salary,
                    age : age
                }
    
                this.props.createEmployee(formData).then(
                    res => console.log(res)
                )
                    .then(this.setState({
                        name : '',
                        salary : '',
                        age : ''
                    })
                );
            }else{
                var formData = {
                    id : this.state.id,
                    name : name,
                    salary : salary,
                    age : age
                }

                this.props.updateEmployee(formData).then(
                    res => console.log(res)
                ).then(json => alert("Record updated"))
    
            }
            
            //alert(JSON.stringify(formData))
        }else{
            alert("please fill all detail")
        }
    }
    render(){
        const {name} = this.props
        return(
            <View style={styles.container}>
                <TextInput
                    style={styles.input}
                    placeholder='Name'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(name) => this.setState({ name })}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.name}
                />
                <TextInput
                    style={styles.input}
                    placeholder='Salary'
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(salary) => this.setState({ salary })}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.salary}
                    keyboardType="number-pad"
                />
                <TextInput
                    style={styles.input}
                    placeholder="Age"
                    placeholderTextColor="#aaaaaa"
                    onChangeText={(age) => this.setState({ age })}
                    underlineColorAndroid="transparent"
                    autoCapitalize="none"
                    value={this.state.age}
                    keyboardType="number-pad"
                />
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.saveData()}>
                    <Text style={styles.buttonTitle}>{(this.state.id) ? 'Update Employee' : 'Save Employee'}</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    input: {
        height: 48,
        width : '100%',
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: 'white',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 30,
        marginRight: 30,
        paddingLeft: 16
    },
    button: {
        width : '100%',
        backgroundColor: '#788eec',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 20,
        height: 48,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: 'center'
    },
    buttonTitle: {
        color: 'white',
        fontSize: 16,
        fontWeight: "bold"
    },
    footerView: {
        flex: 1,
        alignItems: "center",
        marginTop: 20
    },
    footerText: {
        fontSize: 16,
        color: '#2e2e2d'
    },
    footerLink: {
        color: "#788eec",
        fontWeight: "bold",
        fontSize: 16
    }
})

export default connect(null,{createEmployee,updateEmployee})(AddEmployeesScreen);