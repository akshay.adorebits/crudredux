import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    ActivityIndicator,
    Image,
    TouchableOpacity
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchEmployee,deleteEmployee } from '../actions/Employees';
//import * as Actions from '../actions/Employees';

class HomeScreen extends Component {

    componentDidMount() {

        this.props.fetchEmployee()

    }

    _renderItem = ({ item, index }) => {
        return (
            <View key={index} style={{ flexDirection: 'row', backgroundColor: '#fff', marginBottom: 2, marginLeft: 7, height: 80, marginRight: 5 }}>
                <View style={{ paddingRight:5,paddingLeft:5 }}>
                    <Image source={{ uri: item.url }} style={{ height: 70, width: 70, backgroundColor: '#eeeeee', marginTop: 6 }} />
                </View>
                <View style={{ flexGrow: 1, flexShrink: 1, alignSelf: 'center', paddingRight: 5 }}>
                    <Text style={{ fontSize: 15 }}>{item.employee_name}</Text>
                    <View style={{flexDirection:"row"}}>
                        <Text>Salary : {item.employee_salary}</Text>
                        <Text style={{marginLeft : 50}}>age : {item.employee_age}</Text>
                    </View>
                    <View style={{flexDirection:'row'}}>
                        <TouchableOpacity style={{marginTop : 10}}
                            onPress={()=> this.props.navigation.navigate('AddEmployeesScreen',{employee:item,edit:true})}>
                            <Text style={{color:'blue',fontSize:17}}>Edit</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginTop : 10,marginLeft:20}}
                            onPress={() => this.props.deleteEmployee(item.id)}>
                            <Text style={{color:'blue',fontSize:17}}>Delete</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }

    render() {

        const { employees, isFetching } = this.props.employees

        if (isFetching) {
            return (
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    <ActivityIndicator size={'large'} />
                </View>
            )
        } else {
            return (
                <View>
                    <FlatList
                        data={employees}
                        renderItem={this._renderItem}
                        keyExtractor={item => item.id.toString()}
                    />
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => this.props.navigation.navigate('AddEmployeesScreen')}
                        style={{position: 'absolute',
                        width: 50,
                        height: 50,
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        right: 10,
                        bottom: 30}}
                    >
                        <Image
                            source={{
                                uri:
                                    "https://reactnativecode.com/wp-content/uploads/2017/11/Floating_Button.png",
                            }}
                            style={{resizeMode: 'contain',
                            width: 50,
                            height: 50}}
                        />
                    </TouchableOpacity>
                </View>
            )
        }

    }

}

function mapStateToProps(state) {
    return {
        employees: state.employeeReducer
    }
}

function mapDispatchToProps(dispatch) {
    return {
        ...bindActionCreators({ fetchEmployee,deleteEmployee }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)
