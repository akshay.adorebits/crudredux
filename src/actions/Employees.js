import { FETCH_EMPLOYEE_SUCCESS, ADD_EMPLOYEE_SUCCESS, UPDATE_EMPLOYEE_SUCCESS,DELETE_EMPLOYEE_SUCCESS } from '../Utils/constants';

export function fetchEmployee() {

    return (dispatch) => {
        return(fetch('http://dummy.restapiexample.com/api/v1/employees'))
        .then(res => res.json())
        .then(json => {

            return(dispatch(getEmployeeSuccess(json.data)))
        })
        .catch(err => console.log(err))
    }
}

export function getEmployeeSuccess(data) {
    
    return {
        type: FETCH_EMPLOYEE_SUCCESS,
        data
    }
}

export function createEmployee(data){
    console.log(data);
    return (dispatch) => {
        return(fetch("http://dummy.restapiexample.com/api/v1/create",
        {method:"POST",
            headers: {
                Accept: 'application/json'
            },
            body:data
        })).then(res => res.json())
        .then(json => {
            return (dispatch(addEmployeeSuccess(json)))
        })
    }
}

export function addEmployeeSuccess(data){
    return {
        type : ADD_EMPLOYEE_SUCCESS,
        data
    }
}

export function updateEmployee(data){
   // console.log(data)
    // console.log(data)
    return (dispatch) => {
        return(fetch("http://dummy.restapiexample.com/public/api/v1/update/"+data.id,
        {method:"PUT",
            headers: {
                Accept: 'application/json'
            },
            body:data
        })).then(res => res.json())
        .then(json => {
            return (dispatch(uploadEmployeeSuccess(json)))
        })
    }
}

export function uploadEmployeeSuccess(data){
    console.log(data)
    return {
        type : UPDATE_EMPLOYEE_SUCCESS,
        data
    }
}
export function deleteEmployee(id){
    return (dispatch) => {
        return(fetch("http://dummy.restapiexample.com/public/api/v1/update/"+id,
        {method:"GET",
            headers: {
                Accept: 'application/json'
            }
        })).then(res => res.json())
        .then(json => {
            return (dispatch(deleteEmployeeSuccess(json)))
        })
    }
}

export function deleteEmployeeSuccess(data){
    return {
        type : DELETE_EMPLOYEE_SUCCESS,
        data
    }
}