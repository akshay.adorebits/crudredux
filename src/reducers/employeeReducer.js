import { FETCH_EMPLOYEE_SUCCESS, FETCHING_EMPLOYEE,ADD_EMPLOYEE_SUCCESS, UPDATE_EMPLOYEE_SUCCESS,DELETE_EMPLOYEE_SUCCESS} from '../Utils/constants';

const initialState = {
    employees: [],
    isFetching: false,
    error: false
}

export default function todosReducer(state = initialState, action) {
    console.log(action.type)
    console.log(action.data);
    switch(action.type) {
        case FETCHING_EMPLOYEE:
            return {
                ...state,
                isFetching: true
            }
        case FETCH_EMPLOYEE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                employees: action.data
            }
        case ADD_EMPLOYEE_SUCCESS:
            return {
                ...state,
                employees : action.data
            }
        case UPDATE_EMPLOYEE_SUCCESS:
            return {
                ...state,
                employees : action.data
            }
        case DELETE_EMPLOYEE_SUCCESS:{
            return {
                ...state,
                employees : action.data
            }
        }
        default:
            return state
    }
}
