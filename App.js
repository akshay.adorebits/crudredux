import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RootNavigation from './src/navigation/navigation';
import { Provider} from 'react-redux'

import { createStackNavigator } from '@react-navigation/stack';
import configureStore from './src/configureStore';
import HomeScreen from './src/screens/HomeScreen';
// import { createStore } from 'redux';

// import noteReducer from './src/reducers/noteReducer';

export const getStore = configureStore();
const Stack = createStackNavigator();

// const MyNavigation = () => {
//   return(<NavigationContainer>
//     <Stack.Navigator>
//       <Stack.Screen
//         name="Home"
//         component={HomeScreen}
//         options={{ title: 'Welcome' }}
//       />
//     </Stack.Navigator>
//   </NavigationContainer>)
// };
class App extends React.Component{
  render(){
    return(
      <Provider store={getStore}>
        <NavigationContainer>
          <RootNavigation />
        </NavigationContainer>
        {/* <MyNavigation /> */}
      </Provider>
    )
  }
}

export default App;